\label{sec:intro}
One of the key promises of Service Oriented Computing (SOC) is the
delivery of services as composable software units amenable to integration
in a multitude of applications and composite services.  However, building
composite applications is still a challenge if we consider the various
functional and non-functional correctness properties that services should
offer and the compatibility issues that arise.

Most efforts to address this problem focus on the compatibility of
service descriptions and their semantic interoperability~\cite{ADH11}.
Although compatibility of services at this level is necessary, it is
not sufficient for combining them in a correct composition.  Adequate
technical approaches are needed to reveal behavioral incompatibilities,
i.e. flaws in service interactions that cause unexpected service behavior.
Correctness depends on several concerns, including the choice between
synchronous and asynchronous interaction, atomicity, concurrency and so on.
Languages for service orchestrations and service choreographies provide
high-level primitives and constructs to efficiently define complex service
interactions.  However, they lack support for guaranteeing behavioral
correctness, which is related to safety and liveness goals for the composite
service.

Numerous works try to address this problem with verification by model
checking~\cite{BBG07}.  These approaches work on an abstract model
representation extracted from the service composition program.  The
lack of standardized and formally-defined execution semantics for the
used composition languages can lead to debatable analysis results, as
a consequence of implicit and possibly divergent assumptions during
the model extraction (numerous differences between various WS-BPEL
implementations are reported in~\cite{LPT12}).  Moreover, the
structure of the service composition in the program source is lost and
backwards traceability of the verification findings is not possible.
This happens because formalisms based on automata, petri-nets or
process algebras lack a sufficiently expressive set of composition
primitives, appropriate for a model representation that preserves the
service composition structure.

We propose a rigorous approach for the analysis and design of service
compositions based on the BIP (Behavior, Interaction, Priority)
component modeling framework~\cite{BBB11}.  In the core of the BIP
framework lies a powerful executable modeling language with formally
defined operational semantics and mathematically proven expressiveness
properties ~\cite{BS08}.  In BIP, systems are modeled by superposing
three distinct layers.  The lower layer (behavior) consists of a set
of atomic components representing transition systems.  Interactions
between components are specified by connectors in the middle layer.
Each connector is defined as a relation between ports equipped with
synchronization types.  The highest layer (priorities) is used to
enforce scheduling policies applied to interactions.

Our approach elaborates on recent trends in the field of language
technology~\cite{HUD98,HOR08} to introduce a structure-preserving
translation, for embedding WS-BPEL 2.0 service compositions into BIP.
The embedding is defined as a structural representation of the BPEL
primitives and constructs, in terms of the BIP language elements.  The
translation involves an explicit definition of the BPEL execution
semantics in the BIP semantic model.  For this purpose, we are based
on the WS-BPEL standard ~\cite{BPEL20} and other related
works~\cite{FAHL05,STAH05,AALS07}.  In effect, two shortcomings of
conventional model checking are addressed: structure preservation and
explicit composition execution semantics.  As a proof of concept for
the effectiveness of the proposed technique, we present a sample BPEL
program with an expected safety property and we provide the analysis
results obtained with the BIP tools.

The rest of the paper is organized as follows.
Section~\ref{sec:bpelsyntax} summarizes the WS-BPEL abstract syntax
and execution semantics.  Section ~\ref{sec:biplang} introduces the
BIP language and analysis framework that provides novel verification
techniques, which avoid the problem of state space explosion.  In
section~\ref{sec:BIPrep}, we present the structural representation of
BPEL processes in BIP.  Section~\ref{sec:codegen} highlights our
structure-preserving embedding of WS-BPEL into BIP and
section~\ref{sec:application} reports the analysis results for a BPEL
application.  Section~\ref{sec:related} considers related work and 
section~\ref{sec:lessonslearned} exposes the lessons learned.
The paper concludes with summarizing remarks and future research
directions.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "icws_master"
%%% End:
