\label{sec:BIPrep}

BPEL activities are transformed into BIP components. Atomic components
are used for non--decomposable activities (all basic BPEL activities except
\emph{assign}) whereas activities with complex structure (e.g
\emph{sequence} and \emph{flow}) are modeled by compound
components. Moreover, we employ atomic components to model access to
shared variables and links used for explicit activity synchronization.

Events that occur in all activity types are shown in
\figurename~\ref{fig:basicInterface}. Parent activities propagate
\emph{start, disable, term} and \emph{interrupt} events to their
children, while child activities inform their parent about their
current state through the \emph{done, disabled, termed} and
\emph{fault} ports.  These ports constitute the minimum interface of
an activity component. Activity specific behavior is defined by
activity specific transitions and ports that replace the arc
{\it[execute]} connecting the $ready$ and $done$ states.
\figurename~\ref{fig:copy} illustrates an assign activity component
that encloses two copy components that copy messages with one part.
The copy components concurrently read the \emph{from} part through the
readVar port and synchronize on the assignment of the \emph{to} part
through the writeVar port. The readCopy1 and readCopy2 ports carry the
incoming data and the variable uninVar which signals if some data was
not initialized.  The assign port carries the outgoing data of the
assign component.  A fault is thrown by a copy component if the value
of uninVar is true, an event that terminates the execution of the
assign component.  The connectors of the rest of the minimum interface
ports are omitted in \figurename~\ref{fig:copy}.


\subsection{Scope}
\label{sub:scope}

A pictorial representation of the scope component (SC) is shown in
\figurename~\ref{fig:scopeConnect}. We use different line styles to
unambiguously indicate connected ports. A scope is modeled by a
compound component that contains \begin{inparaenum}[(i)]
\item the activity of the scope (MA)
\item a component modeling event handlers (EH), that reacts to received
  messages and timer events
\item a termination handler (TH) which is invoked when a fault in some
  other activity is raised causing the termination of the scope
\item a component modeling fault handlers (FH) which executes an
  activity in response to a fault raised in the scope and
\item a compensation handler  contained in the (CH) which is invoked when a fault in some other activity is raised, \emph{if} the scope is
  completed
\end{inparaenum}. In addition, a scope contains a data access manager
(DAM) component which models access to variables, partner links and correlation sets
declared in the scope.  The constructs for TH, FH and CH are expressed
in the \emph{fcts} rule of BPEL's abstract grammar. For isolated
scopes, the DAM component models concurrent read and write access to
the variables and the partner links it manages.


Normal execution begins with an invocation of the \emph{start} port
that subsequently invokes the \emph{start} port of the MA and EH
components. If a fault occurs in the BPEL process, which has been propagated to
an enclosing scope, either all
constituent components are disabled (\emph{disable} port) or the
\emph{term} port of the scope is invoked. Upon the invocation of the
\emph{term} port, either
\begin{inparaenum}[(i)]
\item the \emph{term} port of MA or EH is invoked
  triggering the \emph{disable} port of the FH and CH components
  ,since exception handling is no longer needed
\item the \emph{term} port of the TH component is invoked to terminate the
  execution of its contained activity, or
\item the \emph{term} port of the CH component is invoked.
\end{inparaenum}
The \emph{start} port of the TH component is triggered when all
constituent components of the scope have enabled the \emph{done} or
\emph{termed} port signaling they have ended.

The \emph{done} port signals that the MA and EH components have
completed their activity without faults or an occurred fault has been
handled by the FH component without throwing another fault. The
\emph{reverse} port of the scope is enabled if no faults have occurred
and the \emph{done} port is enabled. It is requested by the FH, CH or
TH of the enclosing scope and it invokes the \emph{start} port of the
CH component. Faults raised during compensation handling are
propagated through the \emph{faultComp} port to the component which
requested the compensation.

Faults with associated fault handlers invoke the \emph{faulth}
port. All components except the one which raised the fault are either
disabled or terminated. Finally, the fault handler is started. Faults
without an associated fault handler invoke the \emph{fault} port which
propagates the error to the enclosing scope. In either case, the scope
is considered finished.

The exit BPEL activity abruptly terminates a BPEL process. When such
an activity in the scope is executed, the \emph{exit} port is invoked and the
\emph{interrupt} port of the constituent components are also invoked,
effectively terminating all components in the scope. Abrupt termination is
propagated through the \emph{exit}
port to the enclosing component, eventually reaching the root of the component hierarchy.


\subsection{Event handlers}
\label{sub:handlers}

Scopes may contain handlers for many message types. A different
component type is declared for each available message type. In BPEL,
an event handler is instantiated for each received message. However,
the number of received messages is known only at run-time. For state
exploration purposes it suffices to assume the minimum number of
handlers materializing all possible interactions. We model the handler
of each message type as a compound component enclosing two instances
of the same component type that are executed in parallel.


% Each instance is represented by a \emph{sequence} of a
% \emph{receive} and a \emph{scope} activity components.


\subsection{Parallel execution}
\label{sub:flow}
Flow activities coordinate the parallel execution of enclosed
activities.  The BIP flow activity component contain components
corresponding to the encompassed activities and an atomic component
coordinating access to the link values used for synchronization. An
activity that conditions the execution of other activities (i.e. link
source) has an attached atomic component that assigns a value to the
associated link, when the activity is completed. On the other hand, an
activity that is conditioned upon at least one other activity's
execution (i.e.  link target) has an attached atomic component that
initiates the activity after the link value is set.

\subsection{Variables}
In our analysis all execution paths are explored, hence the concrete
values of BPEL variables can not affect program correctness. To
identify attempts of accessing an uninitialized BPEL variable, a
corresponding BIP variable registers whether the former is
initialized.

%  in BIP are assigned binary values indicating whether
% they are initialized, or not.  Although it is important to catch
% attempts of accessing uninitialized variables, our analysis is not
% dependent on the concrete variable values, since it explores all the
% possible execution paths.  By considering only statically correct BPEL
% processes, we expect that the two sides of an assignment will be of a
% matching type, expressions will evaluate to values of the expected
% data type and XML schema validation and selection operations on
% variables will never fail. We also consider that inbound messages are
% of the expected WSDL type, which leads to correct implicit assignments
% to the input variables.

\subsection{Message exchange activities and correlation sets}
Constraints for enabling a message exchange activity are imposed by
partner links, message exchange rules and correlation sets. In BIP,
these constraints are modeled by boolean variables stored in the DAM
component. Moreover, the BIP representation of correlation sets
determines violations of correlation rules.

% Violations of correlation rules depend on the value of the
% BIP variable associated with the correlation set. 

% The value of the associated variable of a correlation set
% determines if an exception is raised due to violation of correlation
% rules.




% Link related actions in activities are performed by atomic components
% ($LSrc$ and $LTrg$), which are \emph{wrapped} along with the activity
% component in one compound component. For instance, $LSrc$ performs
% evaluation of the \emph{transition condition}, according to which it
% sets the link's value. It starts its execution when the activity
% component has been completed, terminated or faulted and interacts with
% the $LK$ component to set the links after the aforementioned
% events. Upon completion of the $LSrc$ the \emph{wrapper} component is
% considered completed or terminated, or can export the fault. For the
% latter case, an atomic component is included in the \emph{wrapper}
% component, which exports the raised fault upon completion of the
% $LSrc$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "icws_master"
%%% End:
