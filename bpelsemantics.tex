\label{sec:bpelsyntax}

\newcommand{\grule} [1] {(\emph{#1})}
\newcommand{\idtype}[1] {$ID_{#1}$}
\newcommand{\etype} [1] {$E_{#1}$}

% \uline{important}
% \uuline{urgent}
% \uwave{boat}
% \sout{wrong}
% \xout{removed text is shown here}

BPEL processes are stateful, loosely coupled interactions of web
services that exchange information via messages. The only restriction
imposed on these interactions is that the structure of the exchanged
information (e.g message field names and types) must adhere to a
provided specification that conforms to the WSDL schema
\cite{WSDL}. \figurename ~\ref{fig:Syntax} outlines the syntax of
BPEL. A process is defined by the topmost scope, which contains
variable declarations \grule{var} for storing the process state. Other
declarations include partner links \grule{plink} for defining service
connections, correlation sets \grule{corrset} for selecting the
message receiver activity and message exchange rules \grule{msge} for
disambiguation of replies to pending requests.

Incoming events are processed by event handlers \grule{evhndlr}
declared in a scope. The channel characteristics of the received
message, such as the partner link \grule{\idtype{pl}} used for
transmission, the operation \grule{\idtype{op}} responsible for
message processing and the correlation set \grule{\idtype{cs}}
acknowledged by the event handler, determine which of the available
handlers receives the message. Due to the asynchronous communication
model of web services, there is no guarantee that a message is
received on time. Timeout conditions \grule{\etype{date},
  \etype{duration}} for waiting a message are defined with
\emph{onAlarm} rules and an associated activity is started when the
timer fires.

In a dialog between web services, it is often the case that a
participant in a conversation either expects to receive a specific
message \grule{receive} or needs to reply to a previously received one
\grule{reply}. To initiate a conversation, a process uses the
\emph{invoke} activity, which defines the message to be sent and
optionally the expected reply.

Normal execution of web services is on occasions interrupted by
faults. Handlers \grule{fcts} for anticipated faults are declared
inside a scope and attempt to compensate or otherwise respond to an
abnormal situation. Faults are propagated towards the root of the
scope hierarchy until an appropriate handler is found. Unhandled
faults cause the process to terminate.


\setlength{\grammarparsep}{2pt plus 1pt minus 1pt}
\setlength{\grammarindent}{7em}
\setcounter{figure}{0}
\begin{figure}[!t]
\small
\begin{grammar}
<process> ::= <scope>

<scope> ::= ID 'isolated'? <decl>* <evhndlr>* <fcts> <act>

<decl> ::= <var> | <plink> | <corrset> | <msge>

<evhndlr>  ::= ('onEvent' <channel> \idtype{var} \alt 'onAlarm' timeExpr) <scope>

<channel> ::= \idtype{pl}  \idtype{op} \idtype{cs}

<timeExpr> ::= (\etype{date} | \etype{duration})  \etype{duration}?

<fcts> ::=  (\idtype{fault}? <act>)+ <act> <act>

<link> ::= (\idtype{link}+ \etype{bool})? (\idtype{link} \etype{bool})*

<links> ::= ID+

<from> ::= literal | \idtype{pl} uri | <from_to>

<to> ::= \idtype{pl} | <from_to>

<from_to> ::= \idtype{var} part? | xpath

<act> ::= <bact> <link>?

<bact> ::= (('receive' | 'reply') <channel> \idtype{var})
\alt 'invoke' <channel>  \idtype{var} \idtype{var}?
\alt 'assign' (<from> <to>)+
\alt 'wait' (\etype{date} | \etype{duration})
\alt 'compensateScope' \idtype{scope}+
\alt <scope> | 'empty' | 'exit' | 'compensate'
\alt 'throw' \idtype{fault} | 'rethrow'
\alt ('sequence' | 'flow' <links>?) <act>+
\alt ('while' | 'repeatUntil') \etype{date} <act>
\alt 'forEach' \etype{int} \etype{int} \etype{int}? <scope>
\alt 'pick' ('onMessage' <channel> <act>)+ ('onAlarm' <timeExpr>
<act>)*
\alt 'if' \etype{bool} <act> ('elseif' \etype{bool} <act>)* ('else' <act>)?
\end{grammar}
\caption{Abstract syntax of BPEL \label{fig:Syntax}}

\end{figure}

Control flow structures common in many programming languages, such as
assignment \grule{assign}, sequential execution of commands
\grule{sequence}, conditional execution \grule{if, pick}, repetition
\grule{forEach, while, repeatUntil} and exceptions \grule{throw,
  rethrow, compensate, compensateScope} are expressed in BPEL by
corresponding activities (\emph{act}). Other activities include
process termination \grule{exit}, temporary pause of execution
\grule{wait} and \emph{empty} that does not performs any computation.

Services are executed in parallel by either the \emph{forEach} or the
\emph{flow} activity. Identical copies of an activity's scope execute
in parallel and optionally with concurrency guarantees, when the
attribute \emph{parallel} of the \emph{forEach} activity is set to
true. Links in a \emph{flow} activity, serve the purpose of explicit
synchronization between the concurrent activities in the
\emph{flow}. The execution of an activity with dependency on a link is
suspended until the activity it depends on assigns a value to the
link.

Activities have access to variables declared in their enclosing
scopes.  Concurrency guarantees for access on the declared variables
and partner links are provided for scopes with \emph{isolated}
attribute set to yes.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "icws_master"
%%% End:
