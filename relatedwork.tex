\label{sec:related}
We have introduced a structure-preserving embedding of WS-BPEL into
BIP based on a structural representation of BPEL orchestrations.  In
order to place our proposal in the wide spectrum of formal methods for
the analysis of service compositions, we adopt the comparison
framework of~\cite{BBG07}.  In that article, the authors review the
pros and cons of 35 published related works classified in three
categories of semantic models, namely automata or labeled transition
systems, Petri-nets and process algebras.  Evaluation is based on the
provided support for the analysis of three service composition
characteristics: (i) language features guaranteeing the continuity of
service delivery termed as connectivity support, (ii) correctness and
(iii) Quality of Service (QoS).

The authors conclude that few of the considered formal methods address
the connectivity characteristic in a satisfactory way and they
specifically emphasize that exception handling and compensations are
supported by a limited number of proposals.  Only six of these works
explicitly propose a model checking approach for analyzing correctness
with respect to safety and liveness properties.  Our approach can be
compared with these related works as follows: (i) BIP offers more
advanced correctness analysis techniques~\cite{BGLN11} that avoid the
scalability limitations of conventional model checking and (ii) our
language embedding for WS-BPEL 2.0 and the transformation
of~\cite{FERR04} for BPEL4WS 1.1 are the only works, which attempt to
preserve the structure of the service composition program.  Regarding
QoS, BIP has been also used in the analysis of timing behavior and it
comes with a tool~\cite{BBB11} that supports performance evaluation.

For considering the effectiveness of BIP in comparison with related process algebraic approaches we recall
\cite{LM07}, where the authors realized a semantic gap between WS-BPEL
2.0 and the $\pi$--calculus. They recognize that the notion of global
state over BPEL computations, the message passing and the combination
of sequencing with concurrency create interleaving and name binding
behavior that cannot be faithfully represented in $\pi$--calculus.  As
a consequence of these findings, they extend $\pi$--calculus with a
transactional construct, in order to provide formal semantics for the 
BPEL activities. In \cite{FERR04}, a two--way
mapping is introduced between BPEL4WS and the LOTOS process algebra.
The authors claim that LOTOS has the expressive power to structurally
represent BPEL processes, due to its compositionality semantics.
However, LOTOS lacks a primitive analogous to the broadcast connector
of BIP.

In order to explain the difference between the process algebra setting
and the BIP approach, we refer to \cite{BS08}.  In process algebras we
can use a series of operators with which processes evolve.  In BIP 
components are characterized by their behavior (labeled transitions)
and composition of behaviors takes place by means of interaction
models and priority models, which essentially perform only memoryless
coordination of behavior (the behavior is not modified when components
execute some transition).  With respect to a notion of expressiveness
that characterizes the ability of some framework to coordinate
components, process algebras have been shown to be less expressive than BIP.

Finally, it is worth comparing BIP with high-level modeling languages
for component-based systems. One language that has been used for modeling compositional construction of web services is Reo~\cite{TASH07}. Reo formal
semantics has been defined with constrained automata, which however cannot preserve service composition structure,
due to lack of powerful coordination operators, such as those in BIP. Another difference is that connectors in Reo are statefull, allowing for coordination behavior that does not preserve essential information about atomic behavior in components.
