\label{sec:codegen}
\algrenewcommand{\algorithmiccomment}[1]{\hskip1em \# #1}
\renewcommand{\alglinenumber}{\footnotesize}
\algblockdefx{StartCom}{EndCom}[1]{\textbf{compound type} #1}{\textbf{end}}
\algblockdefx{StartCon}{EndCon}[1]{\textbf{connector type} #1}{\textbf{end}}
\algblockdefx{StartAt}{EndAt}[1]{\textbf{atomic type} #1}{\textbf{end}}
\algblockdefx{Function}{EndFun}[3]{#3 \textbf{function} #1(#2)}{\textbf{end function}}
\algblockdefx{ForEach}{EndEach}[1]{\textbf{for each} #1}{\textbf{end for}}
\algblockdefx{ForEachC}{EndEachC}[1]{\Comment {for each #1}}{\Comment{end for}}
\algblockdefx{Program}{EndProg}[2]{\textbf{program} #1(#2)}{\textbf{end program}}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\newcommand{\grt}[1]{\textgreater #1}
\newcommand{\lss}[1]{\textless #1}
\newcommand{\hl}[1]{\lss#1\grt}
\renewcommand{\algorithmicindent}{1em}
\newcommand{\noendtags}[0]{\algnotext{EndEach} \algnotext{EndCase}\algnotext{EndSwitch} \algnotext{EndCase}\algnotext{EndDef} \algnotext{EndEachC} \algnotext{EndIf}}
\newcommand{\emptylist}{\texttt{[]}}

\algblockdefx{Switch}{EndSwitch}[1]{\textbf{switch}(#1)}{\textbf{end switch}}
\algblockdefx{Case}{EndCase}[1]{\textbf{case}: #1}{\textbf{end case}}
\algblockdefx{Default}{EndDef}[1]{\textbf{default}: #1}{\textbf{end default}}

We opt for  a code generation embedding approach \cite{HOR08}, such that
BPEL is integrated into BIP by a translator that parses
BPEL documents and the referenced WSDL descriptions, effectively
converting them into a BIP model.

Translation is achieved by a depth first traversal of the process XML
tree as shown in the algorithm of \figurename ~\ref{alg:TRANSFORM}. 
A post-order visit of the children nodes produces BIP code
fragments, temporary stored in local variables. XML node
properties (e.g node name, attribute names and values) determine the appropriate template
applied for translating the node into BIP code. Templates contain
static text and placeholders. Placeholders are replaced by node
attribute values and code fragments produced by the translation of the
descendant nodes. The implementation of the BIP model for the BPEL
process is produced by recursive composition of the code
fragments.
% However, the BIP language requires that the declaration of port and
% connector types precedes the implementation. Thus, we record in a
% globally accessible data structure information necessary for
% generating type declarations.

The execution semantics of a BPEL program is obtained by composing the
structural representations in BIP of the individual BPEL
constructs. The semantics of each construct is independent of the
semantics of the referenced constructs. For example, the semantics of
the flow statement is independent of the semantics of the contained
activities. Compositionality of semantics in BIP is possible due to
the separation of behavior from the interactions.
%\setcounter{figure}{5}
\begin{figure}[h]
\footnotesize
\begin{boxedalgorithmic}[1]
\Function{TRANSFORM}{$node$}{$text$ \textbf{=}}
\noendtags
\State $vars=\emptylist$ \Comment stores BIP code fragments
    \ForEach{$child \in node.children$}
        \State $vars.append(\Call{Transform}{child)}$
    \EndEach
    \State \textbf{return} $\Call{Apply\_template}{node,vars}$
\EndFun
\end{boxedalgorithmic}
\caption{Transformation of XML subtrees of a BPEL process to
  BIP code fragments \label{alg:TRANSFORM}}
\end{figure}

%\setcounter{figure}{6}
\renewcommand{\algorithmicindent}{0,2em}
\begin{figure}[!h]                      % enter the algorithm environment
\footnotesize
\begin{boxedalgorithmic}[1]   % enter the algorithmic environment
\StartAt{copy\_noIgnMissing\_fromToVar\hl{i}(int id)}
    \State \textbf{data} int var\hl{k} = id \Comment for k = 1 .. parts
    \State \textbf{data} bool uninVar
    \State \textbf{export port} e0b0 \hl{sp} \Comment for sp $\in$ ports of the minim. interface
    \State \textbf{export port} e0b0 faultUninVar
    \State \textbf{export port} readVar(\\
\hskip1em var\hl{k},  \Comment for k = 1 .. parts\\
)
     \State \textbf{export port} writeVar(\\
\hskip1em  var\hl{k},  \Comment for k = 1 .. parts\\
)
    \State \textbf{place} INIT, READY, COPY, DONE, TERMED, DISABLED
    \State \textbf{initial to} INIT
\State\Comment .. a sample of state transitions
\State \textbf{on} readVar \textbf{from} READY \textbf{to} COPY
\State \textbf{on} faultUninVar \textbf{from} COPY \textbf{to} TERMED \textbf{provided} uninVar==true
\State \textbf{on} writeVar \textbf{from} COPY \textbf{to} DONE
%\State \textbf{on} interrupt \textbf{from} DONE \textbf{to} TERMED
\EndAt
\end{boxedalgorithmic}
  \caption{Template for translating a \emph{copy} statement with
    \emph{ignoreMissingFromData} set to false \label{alg:copy}}
  \footnotesize
\end{figure}

\figurename ~\ref{alg:copy} shows the template of a BIP code fragment
that models the semantics of the copy statement. It accepts two input
parameters:
\begin{inparaenum}[(i)]
\item a counter which is incremented every time a template is applied
  and
\item the multitude of message parts affected by the copy
  statement\end{inparaenum}. Template lines that refer to placeholders
with values derived from a collection, are repeated for every value in
the collection when the template is evaluated. For example, line 2 of
the template is appended four times when the copy operation affects
four message parts.

The template producing code for an assign activity component
is presented in \figurename ~\ref{alg:assign}. It accepts three input
parameters:
\begin{inparaenum}[(i)]
\item a counter which is incremented every time a template is applied
  \item a list of component types instantiated for each encompassed
    copy statement and
  \item a list containing the multitude of message parts affected by
    each copy statement.
\end{inparaenum}
The connector types for transferring data to the copy component, named
as RV\hl{parts[cp]} in the assign template, are produced by the
template shown in \figurename~\ref{alg:connector}.  The template is
parametrized by the number of message parts accessed by the copy
component.


%\setcounter{figure}{8}

\renewcommand{\algorithmicindent}{0,2em}
\begin{figure}[!h]                      % enter the algorithm environment
\footnotesize
\begin{boxedalgorithmic}[1]   % enter the algorithmic environment
\StartCom{assign\hl{i}}
    \State \textbf{component} \hl{copy[cp]}  \hskip0.5em C\hl{cp}  \Comment for cp = 1 .. size(copy)
    \State \Comment ... sample connectors ...
    \State \textbf{connector} RDV\hl{size(copy)} \hskip0.5em start1(\\
\hskip1em C\hl{cp}.start,  \Comment for cp = 1 .. size(copy)\\
\hskip1em )
\State \textbf{connector} RV\hl{parts[cp]}  \hskip0.5em readCopy\hl{cp}1(C\hl{cp}.readVar)\\
     \Comment for cp = 1 .. size(copy)
\State \textbf{connector} WV\hl{total}  \hskip0.5em assign1( \\
    \hskip1em \Comment total is the sum of the ``parts'' list  \\ %$Parts=\sum_{cp=1}^{size(copy)}{parts[cp]}$ \\
 \hskip1em C\hl{cp}.writeVar, \Comment for cp = 1 .. size(copy)\\
\hskip1em   )
  \State \textbf{connector} RDV\hl{size(copy)}  \hskip0.5em
  faultUninVar1( \\
  \hskip1em C1.faultUninVar,
  \State \hskip1em  C\hl{cp}.term, \Comment for cp = 2 .. size(copy) \\
   \hskip1em  )
    \State \Comment ... sample of exported ports ...
    \State \textbf{export} \textbf{port} readCopy\hl{cp} \hskip0.5em \textbf{is} readCopy\hl{cp}1.xpr
    \\ \hskip1em \Comment for cp = 1 .. size(copy)
    \State \textbf{export} \textbf{port} assign \textbf{is} assign1.xpr
\EndCom
\end{boxedalgorithmic}
  \caption{Template of assign component \label{alg:assign}}
  \footnotesize
\end{figure}

\renewcommand{\algorithmicindent}{0,2em}
\begin{figure}[!h]                      % enter the algorithm environment
\footnotesize
\begin{boxedalgorithmic}[1]   % enter the algorithmic environment
\StartCon{RV\hl{parts} (e\hl{parts}b p)}
 \State define [p]
 \State data int tmp\hl{i} \Comment for i = 1 .. parts
 \State data bool tmp\hl{parts + 1}
  \State on p down \{
 \State  \hskip1em p.msg\hl{i} = tmp\hl{i}; \Comment for i = 1 .. parts + 1 \\
 \}
 \State export port e\hl{parts}b xpr(
 \State \hskip1em tmp\hl{i}, \Comment for i = 1 .. parts
 \State \hskip1em tmp\hl{parts})
\EndCon
\end{boxedalgorithmic}
  \caption{Template for a connector used for transferring data in the copy component \label{alg:connector}}
  \footnotesize
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "icws_master"
%%% End:
